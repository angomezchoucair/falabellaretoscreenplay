package co.com.falabella.task;

import co.com.falabella.userInterface.MedioDePagoPage;
import net.serenitybdd.core.pages.WebElementState;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import org.hamcrest.Matcher;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;

public class MediodePago implements Task {
    public static MediodePago onThePage(){
        return Tasks.instrumented(MediodePago.class);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.should(seeThat(the(MedioDePagoPage.MEDIODEPAGO_TXT), isVisible()));
    }

}
