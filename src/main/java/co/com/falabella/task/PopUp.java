package co.com.falabella.task;

import co.com.falabella.userInterface.PopUpPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class PopUp implements Task {
    public static PopUp onThePage(){
        return Tasks.instrumented(PopUp.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(PopUpPage.VERBOLSA_POP));
    }
}
