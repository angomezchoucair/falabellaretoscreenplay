package co.com.falabella.task;

import co.com.falabella.userInterface.IrAcomprarPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class IrAComprar implements Task {
    public static IrAComprar onThePage(){
        return Tasks.instrumented(IrAComprar.class);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(IrAcomprarPage.IRACOMPRAR_BTN));

    }
}
