package co.com.falabella.task;

import co.com.falabella.userInterface.DireccionPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

public class Direccion implements Task {

    public static Direccion onThePage(){
        return Tasks.instrumented(Direccion.class);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Scroll.to(DireccionPage.DIRRECION_INPUT).andAlignToTop(),
                Enter.theValue("Carrera 15 bb # 34 d 49").into(DireccionPage.DIRRECION_INPUT),
                Enter.theValue("Casa").into(DireccionPage.TIPOVIVIENDA_INPUT),
                Click.on(DireccionPage.INGRESARDIRECCION_BTN));
    }
}
