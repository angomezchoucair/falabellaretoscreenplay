package co.com.falabella.task;

import co.com.falabella.userInterface.ProductoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Producto implements Task {
    public static Producto OnThePage(){
        return Tasks.instrumented(Producto.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(ProductoPage.EQUIPO_IMG),
                Click.on(ProductoPage.AGREGAR_BTN));

    }
}
