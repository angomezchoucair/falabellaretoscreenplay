package co.com.falabella.task;

import co.com.falabella.userInterface.OpcionesDespachoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

public class OpcionesDespacho implements Task {
    public static OpcionesDespacho onThePage(){
        return Tasks.instrumented(OpcionesDespacho.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Scroll.to(OpcionesDespachoPage.CONTINUAR_BTN).andAlignToTop(),
                Click.on(OpcionesDespachoPage.CONTINUAR_BTN));
    }
}
