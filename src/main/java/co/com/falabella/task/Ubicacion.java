package co.com.falabella.task;

import co.com.falabella.userInterface.UbicacionPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Ubicacion implements Task {
public static Ubicacion onThePage(){
    return Tasks.instrumented(Ubicacion.class);
}
    @Override
    public <T extends Actor> void performAs(T actor) {
    actor.attemptsTo(Scroll.to(UbicacionPage.DEPARTAMENTE_INPUT).andAlignToTop(),
            SelectFromOptions.byVisibleText("ANTIOQUIA").from(UbicacionPage.DEPARTAMENTE_INPUT),
            SelectFromOptions.byVisibleText("MEDELLIN").from(UbicacionPage.CIUDAD_INPUT),
            SelectFromOptions.byVisibleText("MEDELLIN").from(UbicacionPage.BARRIO_INPUT),
            Click.on(UbicacionPage.CONTINUAR_BTN));
    }
}
