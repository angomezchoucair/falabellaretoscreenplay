package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class ProductoPage extends PageObject {
    public static final Target EQUIPO_IMG = Target.the("Elige el radio que desea comprar")
            .locatedBy("//*[@id=\"testId-pod-image-4054663\"]");
    public static final Target AGREGAR_BTN = Target.the("oprime el boton de agregar al carrito")
            .locatedBy("//button[contains(text(),'Agregar a la Bolsa')]");
}
