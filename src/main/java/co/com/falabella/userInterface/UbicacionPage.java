package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class UbicacionPage extends PageObject {
    public static final Target DEPARTAMENTE_INPUT= Target.the("Se selecciona el departamento")
            .locatedBy("//select[contains(option,'Por favor selecciona un departamento')]");
    public static final Target CIUDAD_INPUT = Target.the("Se selecciona la ciudad")
            .locatedBy("//select[@id='ciudad']");
    public static final Target BARRIO_INPUT = Target.the("Se selecciona el barrop")
            .locatedBy("//select[@id='comuna']");
    public static final Target CONTINUAR_BTN = Target.the("Oprime el boton continuar")
            .locatedBy("//body/div[3]/div[1]/div[2]/div[1]/section[1]/section[1]/form[1]/div[1]/div[4]/div[1]/button[1]");

}
