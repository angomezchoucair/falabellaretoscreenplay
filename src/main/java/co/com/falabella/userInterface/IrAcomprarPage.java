package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class IrAcomprarPage extends PageObject {
    public static final Target IRACOMPRAR_BTN = Target.the("Oprime el boton de ir a comprar")
            .locatedBy("//button[contains(text(),'Ir a comprar')]");
}
