package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.falabella.com.co/falabella-co")
public class InicioFalabellaPage extends PageObject {
}
