package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class MedioDePagoPage extends PageObject {
    public static final Target MEDIODEPAGO_TXT = Target.the("valida que se muestre la pantalla de medio de pago")
            .locatedBy("//h2[contains(text(),'Elige tu medio de pago')]");
}
