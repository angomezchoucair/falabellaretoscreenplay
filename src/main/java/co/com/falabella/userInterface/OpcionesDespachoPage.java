package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class OpcionesDespachoPage extends PageObject {
    public static final Target CONTINUAR_BTN = Target.the("Oprime el boton continar")
            .locatedBy("//button[contains(text(),'Continuar')]");
}
