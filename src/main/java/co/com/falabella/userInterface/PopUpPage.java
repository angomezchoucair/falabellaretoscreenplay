package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class PopUpPage extends PageObject {
    public static final Target VERBOLSA_POP = Target.the("selecciona la opcion de ver en carrito de compra")
            .locatedBy("//div[contains(a,'Ver Bolsa de Compras')]");
}
