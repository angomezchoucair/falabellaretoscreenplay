package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class HomePage extends PageObject {

    public static final Target PRODUCTO_IMG = Target.the("imagen del tipo de producto a comprar")
            .locatedBy("//*[@id=\"showcase-Showcase-08181ade-7cbe-465c-9905-2797a7aa47f1-item-0\"]/div/div[3]/a[1]");


}
