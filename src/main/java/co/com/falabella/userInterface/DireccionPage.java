package co.com.falabella.userInterface;

import net.serenitybdd.core.pages.PageObject;

import net.serenitybdd.screenplay.targets.Target;

public class DireccionPage extends PageObject {
public static final Target DIRRECION_INPUT = Target.the("ingresa la dirrecion a la cual se quiere enviar")
        .locatedBy("//input[@id='address']");
public static final Target TIPOVIVIENDA_INPUT = Target.the("Ingresa el tipo de vivienda que es")
        .locatedBy("//input[@id='departmentNumber']");
public static final Target INGRESARDIRECCION_BTN = Target.the("Oprime el boton de ingresar direccion")
        .locatedBy("//button[contains(text(),'Ingresar dirección')]");
}
