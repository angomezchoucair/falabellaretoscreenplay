package co.com.falabella.stepDefinitions;

import co.com.falabella.task.*;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class FalabellaStepDefinitions {

    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Dado("^que el usuario se encuentra en la pagina web y selecciona un articulo$")
    public void queElUsuarioSeEncuentraEnLaPaginaWebYSeleccionaUnArticulo() {
    OnStage.theActorCalled("Andres").wasAbleTo(OpenUp.thePage(), (Home.onThePage()));
    }


    @Cuando("^oprime el boton de agregar a la bolsa$")
    public void oprimeElBotonDeAgregarALaBolsa() {
        OnStage.theActorCalled("Andres").attemptsTo(Producto.OnThePage());
    }

    @Cuando("^seleccion la opcion de ver bolsa en el pop up$")
    public void seleccionLaOpcionDeVerBolsaEnElPopUp() {
    OnStage.theActorCalled("Andres").attemptsTo(PopUp.onThePage());
    }

    @Cuando("^oprime el boton de ir a comprar$")
    public void oprimeElBotonDeIrAComprar() {
    OnStage.theActorCalled("Andres").attemptsTo(IrAComprar.onThePage());
    }

    @Cuando("^llena la inforacion de \"([^\"]*)\",\"([^\"]*)\" y \"([^\"]*)\"$")
    public void llenaLaInforacionDeY(String arg1, String arg2, String arg3) {
    OnStage.theActorCalled("Andres").attemptsTo(Ubicacion.onThePage());
    }

    @Cuando("^ingresa la direccion y oprime el boton ingresar \"([^\"]*)\",\"([^\"]*)\"$")
    public void ingresaLaDireccionYOprimeElBotonIngresar(String arg1, String arg2) {
    OnStage.theActorCalled("Andres").attemptsTo(Direccion.onThePage());
    }

    @Cuando("^oprime el boton continuar$")
    public void oprimeElBotonContinuar() {
    OnStage.theActorCalled("Andres").attemptsTo(OpcionesDespacho.onThePage());
    }

    @Entonces("^se muestra las opciones de pago$")
    public void seMuestraLasOpcionesDePago() {
    OnStage.theActorCalled("Andres").attemptsTo(MediodePago.onThePage());
    }


}
